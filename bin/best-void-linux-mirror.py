#!/usr/bin/env python3
import random
import time
import logging
import urllib.request

log = logging.getLogger(__name__)

# taken from
#  https://github.com/void-linux/xmirror/blob/master/mirrors.yaml
MIRRORS = [
    "https://repo-fi.voidlinux.org/",
    "https://repo-de.voidlinux.org/",
    "https://repo-us.voidlinux.org/",
    "https://repo-fastly.voidlinux.org/",
    "https://mirrors.servercentral.com/voidlinux/",
    "https://mirrors.cicku.me/voidlinux/",
    "https://mirror.ps.kz/voidlinux/",
    "https://mirror.nju.edu.cn/voidlinux/",
    "https://mirrors.bfsu.edu.cn/voidlinux/",
    "https://mirrors.cnnic.cn/voidlinux/",
    "https://mirrors.tuna.tsinghua.edu.cn/voidlinux/",
    "https://mirror.sjtu.edu.cn/voidlinux/",
    "https://repo.jing.rocks/voidlinux/",
    "https://void.webconverger.org/",
    "http://ftp.dk.xemacs.org/voidlinux/",
    "https://mirrors.dotsrc.org/voidlinux/",
    "https://ftp.cc.uoc.gr/mirrors/linux/voidlinux/",
    "https://voidlinux.mirror.garr.it/",
    "https://void.cijber.net/",
    "https://void.sakamoto.pl/",
    "http://ftp.debian.ru/mirrors/voidlinux/",
    "https://mirror.yandex.ru/mirrors/voidlinux/",
    "https://ftp.lysator.liu.se/pub/voidlinux/",
    "https://mirror.accum.se/mirror/voidlinux/",
    "https://mirror.puzzle.ch/voidlinux/",
    "https://mirror.vofr.net/voidlinux/",
    "https://mirror2.sandyriver.net/pub/voidlinux/",
    "https://mirror.clarkson.edu/voidlinux/",
    "https://mirror.0xem.ma/voidlinux/",
    "https://mirror.aarnet.edu.au/pub/voidlinux/",
    "https://ftp.swin.edu.au/voidlinux/",
    "https://voidlinux.com.br/repo/",
    "http://void.chililinux.com/voidlinux/",
]


def main():
    # map each mirror to the throughput of downloading
    # /current/x86_64-repodata
    throughputs = {}
    random.shuffle(MIRRORS)

    for index, mirror in enumerate(MIRRORS):
        assert mirror.endswith("/")
        log.info("testing (%d/%d) %r", index + 1, len(MIRRORS), mirror)
        repodata_url = f"{mirror}current/x86_64-repodata"
        log.info("requesting %r", repodata_url)
        try:
            resp = urllib.request.urlopen(repodata_url, timeout=5)
        except Exception as err:
            log.error("got %r, ignoring", err)
            continue
        log.info("received %d", resp.status)
        if resp.status == 200:
            start_ts = time.monotonic()
            data = resp.read()
            end_ts = time.monotonic()
            downloaded_byte_amount = len(data)
            time_taken = end_ts - start_ts
            log.info(
                "took %.2f seconds to download %d bytes",
                time_taken,
                downloaded_byte_amount,
            )
            throughputs[mirror] = int(downloaded_byte_amount / time_taken)
        else:
            log.error("expected 200, got %d", resp.status)
            throughputs[mirror] = -1

    sorted_mirrors = sorted(
        throughputs.keys(), key=lambda k: throughputs[k], reverse=True
    )
    print("sorted mirrors by throughput")
    for mirror in sorted_mirrors:
        print(mirror, "throughput", throughputs[mirror])


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
