#!/bin/bash

# download and install zig's latest build off master
# (for linux builds only, it is SUPPOSED to be arch-independent. i use x86_64)

# this assumes the output path for zig is /usr/bin/zig and the path for all of
# zig's libraries to be /lib/zig, it uses sudo

# usage: zig-update [mode]
#  mode is optional, if mode is set to "nolib", the script won't copy the
#  /lib/zig directory from the tarball into your system. good for
#  stdlib development
#
# requires: curl, wget, jq, head, tar, sudo, cp, cut, mkdir
# is XDG_CACHE_HOME aware. uses $HOME/.cache as fallback

set -eux
arg1=${1:-none}
old_working_dir=$(pwd)

# shellcheck says arch is unused. i disagree, lol.
arch=$(uname -m)

case $(uname -s) in
    Linux) ZIG_ARCH="linux" ;;
    Darwin) ZIG_ARCH="macos" ;;
    *) ZIG_ARCH="no" ;;
esac

if [ "$ZIG_ARCH" = "no" ]; then
    echo "failed to determine system architecture"
    exit 1
fi

zig_build_url=$(
    curl https://ziglang.org/download/index.json \
    | jq -r ".master.\"$arch-$ZIG_ARCH\".tarball"
)

main_cache_dir=${XDG_CACHE_HOME:-"$HOME/.cache"}
zig_cache_dir="$main_cache_dir/zig-update"
mkdir -pv "$zig_cache_dir"

echo "downloading latest zig from \'$zig_build_url\' to $zig_cache_dir"

cd $zig_cache_dir || (echo "FATAL: failed to cd to cache dir." && exit 1)

wget "$zig_build_url" -O zig.tar.xz || exit 1

echo "extracting tarball and cd-ing to it"

tar_out=$(tar xvf zig.tar.xz 2>&1)
filecount=$(echo "$tar_out" | wc)
echo "extracted $filecount files"
if [ "$ZIG_ARCH" = "linux" ]; then
    out_dir=$(echo "$tar_out" | head -1 | cut -f1 -d"/")
elif [ "$ZIG_ARCH" = "macos" ]; then
    out_dir=$(echo "$tar_out" | head -1 | cut -f2 -d" " | cut -f1 -d"/")
fi
cd $out_dir
echo "current dir: $(pwd), want $out_dir"

if [ "$ZIG_ARCH" = "macos" ]; then
    # now this is an ugly hack
    # $PREFIX required on macos
    echo "macos detected, please set PREFIX manually."
else
    PREFIX="/usr"
fi

echo "installing zig binary to $PREFIX/bin"
sudo cp -v zig $PREFIX/bin

if [ "$arg1" != "nolib" ]; then
    sudo mkdir -p $PREFIX/lib/zig
    echo "copying libraries to $PREFIX/lib"
    sudo cp -vr lib/* $PREFIX/lib/zig/
fi

# cd to the old working directory since we changed it to our cache dir while
# running the script
cd $old_working_dir || (echo "FATAL: failed to cd to pre-script directory." && exit 1)
